package com.wust.testmyjitpack;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.wust.toastlib.yqTaostUtils;

public class MainActivity extends AppCompatActivity {

    private Button btn_show_toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_show_toast = findViewById(R.id.btn_show_toast);
        btn_show_toast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 使用我们的库
                yqTaostUtils.showToast(MainActivity.this);
            }
        });
    }
}